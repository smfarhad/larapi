<?php

namespace App\Services\v1;

use App\Flight;

class FlightService{
    protected $supportedIncludes = [
        'arrivalAirport' => 'arrival',
        'departureAirport' => 'departure'
    ];
    protected $clauseProperties =['status', 'flightNumber'];
                                                                        
    
    public function getFlights($parameters){
        if(empty($parameters)){
            return $this->filterFlights(Flight::all());
        }
        
        $withKeys = $this->getWithKeys($parameters);
        $whereClauses = $this->getWhereClause($parameters);
        
        $flights = Flight::with($withKeys)->where($whereClauses)->get();
        return $this->filterFlights($flights, $withKeys);
        //return $this->filterFlights(Flight::with($withKeys)->get(), $withKeys);
    }
//    public function getFlight($flightNumber) {
//        return $this->filterFlights(Flight::where('flightNumber', $flightNumber)->get());
//    }
    
    protected function filterFlights($flights, $keys =[]) {
        $data = [];
        foreach ($flights as $row) {
            $entry = [
                'flightNumber' => $row->flightNumber,
                'status' =>$row->status,
                'href'=> route('flights.show', ['id'=> $row->flightNumber])
            ];  
            if(in_array('arrivalAirport', $keys)){
                $entry['arrival']= [
                  'datetime' => $row->arrivalDateTime,
                  'iataCode' => $row->arrivalAirport->iataCode,
                  'city' => $row->arrivalAirport->city,
                  'state' => $row->arrivalAirport->state,
                ];
            }
            if(in_array('departureAirport', $keys)){
                $entry['departure']= [
                  'datetime' => $row->departureDateTime,
                  'iataCode' => $row->departureAirport->iataCode,
                  'city' => $row->departureAirport->city,
                  'state' => $row->departureAirport->state,
                ];
            }
            
            $data[] = $entry;
        }
        return $data;
        
    }
    public function getWithKeys($parameters) {
        $withKeys =[];
        if(isset($parameters['include'])){
            $includeParams = explode(',',$parameters['include']);
            $includes = array_intersect($this->supportedIncludes, $includeParams);
            $withKeys = array_keys($includes);
        }
        return $withKeys;
    }
    
    public function getWhereCLause($parameters) {
        $clause = [];
        
        foreach ($this->clauseProperties as $prop) {
            if(in_array($prop, array_keys($parameters))){
                $clause[$prop] = $parameters[$prop];
            }
        }
        return $clause;
    }
    
    
    
}
